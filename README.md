# Personal Config files

Set of utilities to replicate my development environment and keep them updated.

I use Linux Mint Cinnamon version as my base distro.

## Utility Scripts

**Note:** bash scripting is hellish, so don't expect the most efficient or elegant code.

- `fresh_install.sh`: when installing a new system, use this script to set everything up from scratch.
- `sync_dotfiles.sh`: update the configuration files.

## Workflow

I use Neovim as my main editor along with Docker to isolate different development environments.

TODO: decide what to use for personal organization. Some options are:
- org files for organization
- git repo with markdown files serving as org files
- external tool (Asana, Trello, Taiga, Jira, etc)

### Vim/Neovim

#### For reducing `Esc` key usage

- `Alt+o`: open a new line below the one you are currently editing.
- `Alt+A`: append to the end of the current line.
- `Alt+p`: paste at the current insert location.
- `Alt+{h,j,k,l}`: move left, down, up, right.

#### Replace

- `<Leader>r`: replace the word where cursor is standing for the one entered.

#### Quickfix

It's a windows that can be used to show results in a list form, for example, greping content, linting or compilation erros, etc, and traverse through them. There's only one quickfix window per Vim instance.

- `:cope[n]`: open quickfix window.
- `:ccl[ose]`: close quickfix window.
- `:cn[ext]`: display the next error.
- `:cN[ext]` or `:cp[revious]`: display the previous error.
- `:cc [nr]`: go to the highlighted error or to error `[nr]` if given.
- `:cdo %s/foo/bar/g | update`: replace in each valid entry in the quickfix list.
- `:cfdo %s/foo/bar/g | update`: replace in each file in the quickfix list.

Further info: `:help quickfix-window` and `E925`.

#### Location lists

Location lists are like quickfix with the exception that each window can have one location list. All the commands are the same as in the quickfix window except with change the initial `'c'` with a `'l'`.

- `:lope[n]`: same as `:cope[n]`.
- `:lcl[ose]`: same as `:ccl[ose]`.
- `:ln[ext]`: same as `:cn[ext]`.
- `:lN[ext]` or `:lp[revious]`: same as `:cN[ext]` or `:cp[revious]`.
- `:lc [nr]`: same as `:cc [nr]`.
- `:ldo %s/foo/bar/g | update`: same as `:cdo %s/foo/bar/g | update`.
- `:lfdo %s/foo/bar/g | update`: same as `:cfdo %s/foo/bar/g | update`.

Further info: `:help location-list-window` and `E925`.

TODO: check these
- Migrate from silver searcher to [ripgrep](https://github.com/BurntSushi/ripgrep) when upgrading to a more recent Linux Mint version as it is not currently available on the repos.
- [UltiSnips](https://github.com/SirVer/ultisnips)
- [undotree](https://github.com/mbbill/undotree)
- [Gina](https://github.com/lambdalisue/gina.vim): decide if my workflow needs async git
- [tabular](https://github.com/godlygeek/tabular): check screencast

Decided to leave out Language Server Protocol and delegate the language-specific linting/fixing to docker images that contain the appropriate tools.

### Some useful bash utilities that I keep forgetting

Stolen from [here](https://github.com/engineer-man/youtube/blob/master/058/commands.sh).

```console
# 1. redo last command but as root
sudo !!

# 2. open an editor to run a command
ctrl+x+e

# 3. create a super fast ram disk
mkdir -p /mnt/ram
mount -t tmpfs tmpfs /mnt/ram -o size=8192M

# 4. don't add command to history (note the leading space)
 ls -l

# 5. fix a really long command that you messed up
fc

# 6. tunnel with ssh (local port 3337 -> remote host's 127.0.0.1 on port 6379)
ssh -L 3337:127.0.0.1:6379 root@emkc.org -N

# 7. quickly create folders
mkdir -p folder/{sub1,sub2}/{sub1,sub2,sub3}

# 8. intercept stdout and log to file
cat file | tee -a log | cat > /dev/null

# bonus: exit terminal but leave all processes running
disown -a && exit
```
