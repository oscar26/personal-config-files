#!/bin/bash

# Script used when provisioning a new system.
#
# Run this from within the root of the repo

update_upgrade() {
    sudo apt-get update -q
    sudo apt-get upgrade -yq
}

generate_home_folders() {
    local original_dir=$(pwd)
    local folders=(
        'Workspaces'
    )
    cd $HOME
    echo "Creating $HOME folder structure..."
    echo ${folders[@]}
    mkdir -p ${folders[@]}
    cd $original_dir
}

install_essentials() {
    sudo apt-get install -y \
        software-properties-common ubuntu-drivers-common ca-certificates \
        build-essential make curl wget git htop file openssh-server \
        python3-pip python3-setuptools python3-wheel \
        docker.io neovim neofetch ripgrep
    sudo usermod -aG docker $USER
}

install_additional_packages() {
    # Lazydocker
    curl https://raw.githubusercontent.com/jesseduffield/lazydocker/master/scripts/install_update_linux.sh | bash
    # Lazygit
    sudo add-apt-repository --yes ppa:lazygit-team/release
    sudo apt-get update
    sudo apt-get install lazygit
}

sync_dotfiles(){
    chmod u+x sync_dotfiles.sh
    ./sync_dotfiles.sh
}

cleanup() {
    update_upgrade
    sudo apt-get autoremove -yq
    sudo apt-get clean -yq
}

install_all() {
    echo -e "\n\n  --- Starting system configuration --- \n\n\n"
    echo -e "WARNING: the system will restart once everything is set up!!!\n\n\n"

    # Storing the starting pwd if we need to come back to it later
    local starting_dir=$(pwd)

    update_upgrade
    generate_home_folders
    install_essentials
    install_additional_packages

    # WARNING: configuration files must be brought in before
    # attempting using the system for the first time, As some
    # config files must be in place for correct functioning,
    cd $starting_dir
    sync_dotfiles

    # That's all folks!
    cleanup

    echo "Remember to install Bitwarden and Telegram!!!!!"
}

install_all
