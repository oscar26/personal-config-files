#!/bin/bash

## Run this from within the root of the repo

# Setting default editor if not found
bash_config=~/.bashrc
if [[ "$(grep VISUAL $bash_config)" == "" ]]; then
    echo -e "\nexport VISUAL=nvim" >> $bash_config
    if [[ "$(grep EDITOR $bash_config)" == "" ]]; then
        echo "export EDITOR=nvim" >> $bash_config
    fi
fi
# Install git branch prompt
rm -rf ~/.bash-git-prompt
git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt --depth=1
if [[ "$(grep gitprompt.sh $bash_config)" == "" ]]; then
    echo -e "\nif [ -f \"\$HOME/.bash-git-prompt/gitprompt.sh\" ]; then
    GIT_PROMPT_THEME=Single_line_Solarized_Lamda
    GIT_PROMPT_ONLY_IN_REPO=1
    source $HOME/.bash-git-prompt/gitprompt.sh
fi" >> $bash_config
fi
unset bash_config

if [ ! -d "$HOME/.config/nvim" ]; then
    mkdir -p ~/.config/nvim
fi

# We don't want the nvim dir to be deleted, so we copy just the init.vim
echo "Copying 'config/nvim/init.vim' -> '$HOME/.config/nvim/'"
cp -r config/nvim/init.vim ~/.config/nvim/
