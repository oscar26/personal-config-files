" Snippets were stolen from various parts of the web, like:
"
"     https://www.reddit.com/r/vim/comments/7iy03o/you_aint_gonna_need_it_your_replacement_for/dr2qo4k/


" Setting a single autocmd group for automatic actions
augroup autos
    autocmd!
augroup END


"
" ==== Plugins section ====
"

" Nvim config folder path
let s:nvimconfig=$HOME . '/.config/nvim'

" Automatic installation of vim-plug
let s:vimplug=s:nvimconfig . '/autoload/plug.vim'
if empty(glob(s:vimplug))
    silent execute '!curl -fLo ' . s:vimplug . ' --create-dirs
        \ "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"'
    augroup autos
        autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
    augroup END
endif

" Automatic installation of missing plugins
augroup autos
    autocmd VimEnter *
        \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
        \|     PlugInstall --sync | q
        \| endif
augroup END

" Plugin declarations
call plug#begin(s:nvimconfig . '/plugged')
Plug 'vim-airline/vim-airline'
Plug 'mhinz/vim-startify'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'vim-scripts/DeleteTrailingWhitespace'
Plug 'jiangmiao/auto-pairs'
Plug 'alvan/vim-closetag'
Plug 'ajh17/VimCompletesMe'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'romainl/vim-qf'
Plug 'hauleth/asyncdo.vim'
Plug 'sheerun/vim-polyglot'
Plug 'liuchengxu/vim-which-key'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'voldikss/vim-floaterm'
call plug#end()


"
" ==== Python3 ====
"
let g:python3_host_prog = '/usr/bin/python3'


"
" ==== Grep program ====
"
" Search in project
if executable("rg")
    set grepprg=rg\ --vimgrep
    set grepformat^=%f:%l:%c:%m
endif


"
" ==== Vanilla settings (no plugins involved) ====
"

" Leader keys
let g:mapleader = "\<Space>"
let g:maplocalleader = ","

" Set theme
set background=dark

" Enable mouse
set mouse=a

" Set line numbers
set number

" General indentation rules
set tabstop=4
set shiftwidth=4
set expandtab

" Open new split panes to right and bottom
set splitbelow
set splitright

" File navigation. Useful when using find, sfind, etc.
set path+=** " recursive filepath completion
set wildignorecase

set wildignore+=*.swp,*.bak
set wildignore+=*.pyc,*.class,*.sln,*.Master,*.csproj,*.csproj.user,*.cache,*.dll,*.pdb,*.min.*
set wildignore+=*/.git/**/*,*/.hg/**/*,*/.svn/**/*
set wildignore+=*/min/*
set wildignore+=tags,cscope.*
set wildignore+=*.tar.*

" Time for next key stroke
set timeoutlen=500

" Make error format
"
" Check
"   https://vi.stackexchange.com/questions/18846/ignore-lines-in-errorformat
set errorformat+=%f:%l:%c:%m
set errorformat+=%W%f:%l\ %.%#,%Z%m
" Ignore lines not matched by previous expressions
set errorformat+=%-G%.%#

" Vertical split line color
highlight VertSplit ctermbg=234 ctermfg=234 term=none

" Highlight cursor column on current buffer
augroup autos
    autocmd WinLeave * set nocursorcolumn
    autocmd WinEnter * set cursorcolumn
augroup END

" Close documentation window after completion
augroup autos
    autocmd CompleteDone * pclose
augroup END

" Automatically open quickfix/location windows
augroup autos
    autocmd QuickFixCmdPost [^l]* cwindow
    autocmd QuickFixCmdPost    l* lwindow
augroup END

" Refresh buffers when changed by external program
" https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
augroup autos
    " Triger 'autoread' when files changes on disk
    autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif
    " Notification after file change
    autocmd FileChangedShellPost *
      \ echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None
augroup END

" Terminal
augroup autos
    " Enter and leave terminal mode (insert mode) when switching between buffers
    autocmd BufWinEnter,WinEnter term://* startinsert
    autocmd BufLeave term://* stopinsert
    " No line numbers in the terminal
    autocmd TermOpen * setlocal nonumber norelativenumber
augroup END

" MRU command-line completion
function! s:MRUComplete(ArgLead, CmdLine, CursorPos)
    return filter(copy(v:oldfiles), 'v:val =~ a:ArgLead')
endfunction

" MRU function
function! s:MRU(command, arg)
    if a:command == "tabedit"
        execute a:command . " " . a:arg . "|lcd %:p:h"
    else
        execute a:command . " " . a:arg
    endif
endfunction

" MRU commands
command! -nargs=1 -complete=customlist,<sid>MRUComplete ME call <sid>MRU('edit', <f-args>)
command! -nargs=1 -complete=customlist,<sid>MRUComplete MS call <sid>MRU('split', <f-args>)
command! -nargs=1 -complete=customlist,<sid>MRUComplete MV call <sid>MRU('vsplit', <f-args>)
command! -nargs=1 -complete=customlist,<sid>MRUComplete MT call <sid>MRU('tabedit', <f-args>)

" Close quickfix on exit and resize windows when quitting quickfix or VIM
" entirely. Horizontal split line misaligns when quickfix is closed and
" triggers OCD if not handled.
function! s:CountQuickfixTypeWindows()
    let counter = 0
    for win in range(winnr('$'))
        if getwinvar(win+1, '&syntax') == 'qf'
            let counter = counter + 1
        endif
    endfor
    return counter
endfunction

augroup autos
    autocmd WinLeave *
                \  if s:CountQuickfixTypeWindows() == 0
                \|     wincmd p | 1wincmd w | wincmd = | wincmd p
                \| endif
    autocmd VimLeavePre * windo
                \  if getwinvar(winnr(), '&syntax') == 'qf'
                \|     lclose
                \| endif | 1wincmd w | cclose | helpclose | wincmd =
augroup END


"
" ==== Mappings ====
"

" To map <Esc> to exit terminal-mode
tnoremap <Esc> <C-\><C-n>

" To use Ctrl+Alt+{h,j,k,l} to navigate windows from any mode
tnoremap <C-A-h> <C-\><C-N><C-w>h
tnoremap <C-A-j> <C-\><C-N><C-w>j
tnoremap <C-A-k> <C-\><C-N><C-w>k
tnoremap <C-A-l> <C-\><C-N><C-w>l
inoremap <C-A-h> <C-\><C-N><C-w>h
inoremap <C-A-j> <C-\><C-N><C-w>j
inoremap <C-A-k> <C-\><C-N><C-w>k
inoremap <C-A-l> <C-\><C-N><C-w>l
nnoremap <C-A-h> <C-w>h
nnoremap <C-A-j> <C-w>j
nnoremap <C-A-k> <C-w>k
nnoremap <C-A-l> <C-w>l

" To switch last used windows using Ctrl+Alt+p
tnoremap <C-A-p> <C-\><C-N><C-w>p
inoremap <C-A-p> <C-\><C-N><C-w>p
nnoremap <C-A-p> <C-w>p

" Use <C-L> to clear the highlighting of :set hlsearch.
if maparg('<C-L>', 'n') ==# ''
    nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
endif


"" Leader mappings and which-key menu
"
" Although which-key lets me place the mapping in the dictionary itself, I
" prefer to keep them separate in case I want to ditch the plugin in the
" future
let g:which_key_map =  {}

" Top level menu
nnoremap <Leader>t :FloatermToggle<CR>
nnoremap <Leader>n :FloatermNew<CR>
nnoremap <Leader>g :FloatermNew --name=git lazygit<CR>
nnoremap <Leader>d :FloatermNew --name=docker lazydocker<CR>
nnoremap <Leader>py :FloatermNew --name=python3 python3<CR>
" Replace word on cursor
nnoremap <Leader>r :%s/\<<C-r><C-w>\>//g<Left><Left>
" Confirm quit
nnoremap <Leader>qq :conf qa<CR>

let g:which_key_map = {
    \ 'name': '+top level',
    \ 'r': 'replace-word',
    \ 't': 'toggle-terminal',
    \ 'n': 'new-terminal',
    \ 'g': 'lazygit',
    \ 'd': 'lazydocker',
    \ 'py': 'python-REPL',
    \ 'qq': 'confirm-quit',
    \ }
" Which-key expects a map for 'p' as 'py' is defined, so it add its to the
" menu with the name '+prefix'. Mapping the unwanted entry to <nop> removes it
" from the menu
nnoremap <Leader>p <nop>
let g:which_key_map["p"] = 'which_key_ignore'
nnoremap <Leader>q <nop>
let g:which_key_map["q"] = 'which_key_ignore'


" fzf menu
nnoremap <Leader>ff :Files<CR>
nnoremap <Leader>fb :Buffers<CR>
nnoremap <Leader>fc :Colors<CR>
nnoremap <Leader>fm :Marks<CR>
nnoremap <Leader>fw :Windows<CR>
nnoremap <Leader>fh :History<CR>
nnoremap <Leader>fhc :History:<CR>
nnoremap <Leader>fhs :History/<CR>
nnoremap <Leader>fsp :Snippets<CR>
nnoremap <Leader>fcm :Commands<CR>
nnoremap <Leader>fmp :Maps<CR>
nnoremap <Leader>fht :Helptags<CR>
nnoremap <Leader>fft :Filetypes<CR>
" Commands requiring extra input
nnoremap <Leader>frg :Rg<Space>
nnoremap <Leader>fl :Lines<Space>
nnoremap <Leader>fbl :BLines<Space>
nnoremap <Leader>flo :Locate<Space>

let g:which_key_map.f = {
    \ 'name': '+fzf',
    \ 'f': 'files',
    \ 'b': 'buffers',
    \ 'c': 'colors',
    \ 'm': 'marks',
    \ 'w': 'windows',
    \ 'h': 'file-history',
    \ 'hc': 'command-history',
    \ 'hs': 'search-history',
    \ 'sp': 'snippets',
    \ 'cm': 'commands',
    \ 'mp': 'maps',
    \ 'ht': 'help-tags',
    \ 'ft': 'file-types',
    \ 'rg': 'rg',
    \ 'l': 'search-lines-all-buffers',
    \ 'bl': 'search-lines-current-buffer',
    \ 'lo': 'locate',
    \ }

nnoremap <Leader>fr <nop>
let g:which_key_map.f["r"] = "which_key_ignore"
nnoremap <Leader>fs <nop>
let g:which_key_map.f["s"] = "which_key_ignore"


" Make menu
nnoremap <Leader>ma :AsyncDo make<Space>
nnoremap <Leader>mf :AsyncDo make format<CR>
nnoremap <Leader>ml :AsyncDo make lint<CR>
nnoremap <Leader>mt :AsyncDo make test<CR>

let g:which_key_map.m = {
    \ 'name': '+make',
    \ 'a': 'any-target',
    \ 'f': 'format',
    \ 'l': 'lint',
    \ 't': 'test',
    \ }


"
" ==== Plugin configs ====
"

" ==== Airline ====
"
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1

" Higher contrast status line when inactive
let g:airline_theme_patch_func = 'AirlineThemePatch'
function! AirlineThemePatch(palette)
    if g:airline_theme == 'dark'
        for colors in values(a:palette.inactive)
            let colors[2] = 246
        endfor
    endif
endfunction

" Integration with AsyncDo
function! GetAsyncDoRunning()
    if exists('g:asyncdo')
        return 'Running: ' . g:asyncdo.cmd
    endif
    return ''
endfunction

call airline#parts#define_function('asyncdo_status', 'GetAsyncDoRunning')
let g:airline_section_x = airline#section#create(['asyncdo_status'])


" ==== Close HTML tags ====
"
let g:closetag_filenames = '*.html,*.xhtml,*.phtml,*.jinja'


" ==== Delete trailing whitespaces ====
"
" Trim trailing whitespaces before saving
augroup autos
    autocmd BufWritePre * :DeleteTrailingWhitespace
augroup END


" ==== Which key ====
"
call which_key#register('<Space>', 'g:which_key_map')
nnoremap <silent> <leader> :<c-u>WhichKey '<Space>'<CR>
nnoremap <silent> <localleader> :<c-u>WhichKey  ','<CR>


" ==== Floaterm =====
"
let g:floaterm_keymap_new    = '<F7>'
let g:floaterm_keymap_prev   = '<F8>'
let g:floaterm_keymap_next   = '<F9>'
let g:floaterm_keymap_toggle = '<F12>'

let g:floaterm_autoclose = 1
highlight FloatermBorder guibg=black


" ==== Startify ====
"
" Changing lists order
let g:startify_lists = [
    \ { 'type': 'sessions',  'header': ['   Sessions']       },
    \ { 'type': 'files',     'header': ['   MRU']            },
    \ { 'type': 'dir',       'header': ['   MRU '. getcwd()] },
    \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
    \ { 'type': 'commands',  'header': ['   Commands']       },
    \ ]

" Enable session persistance
let g:startify_session_persistence = 1

" Custom header
highlight StartifyHeader ctermfg=13
let s:startify_ascii_header = [
            \ '███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗',
            \ '████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║',
            \ '██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║',
            \ '██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║',
            \ '██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║',
            \ '╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝',
            \ '                                                  ',
            \ '',
            \]
let g:startify_custom_header = map(s:startify_ascii_header +
    \ startify#fortune#boxed(), '"   ".v:val')
